# Prometheus SElinux policy modules

These modules are largely based on work by [georou](https://github.com/georou), but adapted to work with an Ansible git based deploy that you can [see here](https://gitlab.com/stemid-ansible/playbooks/ansible-prometheus.git).

Which practically means that each prometheus component service is deployed within its own /var/opt/directory.

# Build

To build modules on Fedora you need;

* selinux-policy-devel

Then run this to build one module;

    make -f /usr/share/selinux/devel/Makefile alertmanager.pp

# Install

    sudo semodule -i alertmanager.pp

# Important about network ports

These modules do not assign the port contexts required, I am personally using Ansible to do this last part because I want control over which port I end up using.

But here is an example of assigning the default alertmanager ports.

    sudo semanage port -a -t alertmanager_port_t -p tcp 9094
    sudo semanage port -a -t alertmanager_port_t -p udp 9094
    sudo semanage port -a -t alertmanager_port_t -p tcp 9093
